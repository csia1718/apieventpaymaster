const mongoose = require('mongoose');

const EvenementSchema = mongoose.Schema({
	nom: String,
	datDeb: Date,
	datFin: Date,
	pays: String,
	ville: String,
	adresse: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Evenement', EvenementSchema);
// module.exports = mongoose.model('Evenements', EvenementSchema);