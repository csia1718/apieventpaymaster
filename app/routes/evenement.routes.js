module.exports = (app) => {
    const evenements = require('../controllers/evenement.controller.js');

    // Creer evenement
    app.post('/evenements', evenements.create);

    // Recuperer les evenements
    app.get('/evenements', evenements.findAll);

    // Recuperer un evenement avec son id
    app.get('/evenements/:evenementId', evenements.findOne);

    // Metre a jour un evenement avec son id
    app.put('/evenements/:evenementId', evenements.update);

    // Supprimer un evenement avec son id
    app.delete('/evenements/:evenementId', evenements.delete);
}