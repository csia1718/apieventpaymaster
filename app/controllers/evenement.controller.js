
const Evenement = require('../models/evenement.model.js');

// Creer un nouvel evenement
exports.create = (req, res) => {
	// Validation de la requête
    if(!req.body.nom) {
        return res.status(400).send({
            message: "Un evenement ne peut pas être créer sans nom"
        });
    }

    // Création de l'événement
    const evenement = new Evenement({
    	nom: req.body.nom
    });
    if (req.body.datDeb){
    	evenement.datDeb = new Date(req.body.datDeb)
    }
	if (req.body.datFin){
		evenement.datFin = new Date(req.body.datFin)
	}

    // Sauvegarde de l'évenement dans la base de données
    evenement.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Une erreure est survenue lors de la création de l'événement"
        });
    });

};

// Recuperer tous les evenements
exports.findAll = (req, res) => {
	Evenement.find()
    	.then(evenements => {
        	res.send(evenements);
	    }).catch(err => {
	        res.status(500).send({
	            message: err.message || "Une erreure est survenue lors de la récupération des événements"
	        });
	    });
};

// Trouver un evenement avec son id
exports.findOne = (req, res) => {
	Evenement.findById(req.params.evenementId)
	    .then(evenement => {
	        if(!evenement) {
	            return res.status(404).send({
	            	//requête n'a rien trouvé
	                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
	            });            
	        }
	        res.send(evenement);
	    }).catch(err => {
	        if(err.kind === 'ObjectId') {
	        	//requête a planté
	            return res.status(404).send({
	                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
	            });                
	        }
	        return res.status(500).send({
	            message: "Une erreur est survenue lors de la récupération de l'événement " + req.params.evenementId
	        });
	    });
};

// Met a jour un evenement avec son id
exports.update = (req, res) => {
	// Validation de la requête
    if(!req.body.nom) {
        return res.status(400).send({
            message: "Un evenement ne peut pas être sans nom"
        });
    }

    // Trouve un evenement et le met à jour
    Evenement.findByIdAndUpdate(req.params.evenementId, {
        nom: req.body.nom,
		datDeb: req.body.datDeb,
		datFin: req.body.datFin
    }, {new: true}) // met à jour les timestamps
    .then(evenement => {
        if(!evenement) {
            return res.status(404).send({
                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
            });
        }
        res.send(evenement);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
            });                
        }
        return res.status(500).send({
            message: "Une erreur est survenue lors de la mise à jour de l'événement " + req.params.evenementId
        });
    });
};

// Supprimer un evenement avec son id
exports.delete = (req, res) => {
	Evenement.findByIdAndRemove(req.params.evenementId)
	    .then(evenement => {
	        if(!evenement) {
	            return res.status(404).send({
	                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
	            });
	        }
	        res.send({message: "Evenement supprimé avec succés"});
	    }).catch(err => {
	        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
	            return res.status(404).send({
	                message: "Evenement non trouvé avec l'id : " + req.params.evenementId
	            });                
	        }
	        return res.status(500).send({
	            message: "Impossible de supprimer l'événement " + req.params.noteId
	        });
	    });
};